# FrameCave
A web application builder/framework with most needed tools to complete your next web project.


## Features
* Uses latest Symfony LTS
* Admin Panel
* i18n prepared
  * English - default
  * German - secondary, example
* EU Cookie Compliant
* User Administration
* Working Contact page
* Legal Info Examples


## Docs
Will be available at a later point in development.


## License
BSD 3-clause, see [LICENSE](LICENSE) for details.


## Usage (Quick Start)
Will be available at a later point in development.
